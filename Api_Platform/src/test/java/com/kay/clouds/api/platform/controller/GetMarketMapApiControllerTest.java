package com.kay.clouds.api.platform.controller;

import com.google.common.collect.Lists;
import com.kay.clouds.api.platform.controller.GetMarketMapApiControllerTest.Config;
import com.kay.clouds.api.util.testing.TestServerInterface;
import com.kay.clouds.domain.platform.market.Market;
import com.kay.clouds.domain.platform.market.MarketRepository;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import static org.springframework.context.annotation.FilterType.ASSIGNABLE_TYPE;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {Config.class})
public class GetMarketMapApiControllerTest {

    @Configuration
    @ComponentScan(
            basePackages = {"com.kay.clouds.api.platform.controller"},
            useDefaultFilters = false,
            includeFilters = {
                @ComponentScan.Filter(type = ASSIGNABLE_TYPE, value = GetMarketMapApiController.class)
            })
    public static class Config implements UnitTestConfiguration {

        public class MarketData implements Market {

            private String id;
            private String countryCode;
            private String name;
            private String description;
            private String currencyCode;
            private Double latitude;
            private Double longitude;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCountryCode() {
                return countryCode;
            }

            public void setCountryCode(String countryCode) {
                this.countryCode = countryCode;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getCurrencyCode() {
                return currencyCode;
            }

            public void setCurrencyCode(String currencyCode) {
                this.currencyCode = currencyCode;
            }

            public Double getLatitude() {
                return latitude;
            }

            public void setLatitude(Double latitude) {
                this.latitude = latitude;
            }

            public Double getLongitude() {
                return longitude;
            }

            public void setLongitude(Double longitude) {
                this.longitude = longitude;
            }

        }

        @Override
        public MarketRepository marketRepository() {
            return new MarketRepository() {
                @Override
                public void save(Market stock) {
                    //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public List<? extends Market> all() {
                    MarketData data = new MarketData();
                    data.setCountryCode("country");
                    data.setCurrencyCode("currency");
                    data.setDescription("description");
                    data.setId("id");
                    data.setLatitude(8.8);
                    data.setLongitude(9.9);
                    data.setName("name");
                    List<MarketData> dataList = Lists.newArrayList(data);
                    return dataList;

                }

                @Override
                public Market one(String key) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public List<? extends Market> in(List<String> keys) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

            };

        }
    }

    private TestServerInterface server;

    @Autowired
    GetMarketMapApiController controller;

    String url;

    @Before
    public void setUp() {
        url = GetStockListApi.URL;
        server = new TestServerInterface(controller);
    }

    @Test
    public void requestTest() throws Exception {

        String response = server.getResponseString(url);

    }

}
