package com.kay.clouds.api.platform.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kay.clouds.api.util.handling.ApiAuthenticator;
import com.kay.clouds.api.util.handling.ApiLogger;
import com.kay.clouds.domain.platform.index.stock.IndexContainStockRepository;
import com.kay.clouds.domain.platform.market.MarketRepository;
import com.kay.clouds.domain.platform.stock.StockRepository;
import com.kay.clouds.domain.platform.user.UserRepository;
import com.kay.clouds.domain.platform.user.stock.UserFollowStockRepository;
import com.kay.clouds.domain.platform.user.stock.UserLikeStockRepository;
import com.kay.clouds.domain.security.credential.CredentialService;
import java.io.IOException;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;

/**
 *
 * @author Lili
 */
public interface UnitTestConfiguration {

    @Bean
    default ApiAuthenticator apiAuthenticator() throws IOException {
        return new ApiAuthenticator() {
            @Override
            protected Boolean verify(String token) {
                return Boolean.TRUE;
            }
        };
    }

    @Bean
    default ObjectMapper getObjectMapper() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        return mapper;
    }

    @Bean
    default Logger getLogger() throws IOException {
        return ApiLogger.LOGGER;
    }

    @Bean
    default StockRepository stockRepository() {
        return null;
    }

    @Bean
    default UserRepository userRepository() {
        return null;
    }

    @Bean
    default MarketRepository marketRepository() {
        return null;
    }

    @Bean
    default UserFollowStockRepository followRepository() {
        return null;
    }

    @Bean
    default UserLikeStockRepository likeRepository() {
        return null;
    }

    @Bean
    default IndexContainStockRepository indexContainStockRepository() {
        return null;
    }

    @Bean
    default CredentialService credentialService() {
        return null;
    }

}
