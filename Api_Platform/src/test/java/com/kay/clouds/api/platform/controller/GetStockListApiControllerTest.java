package com.kay.clouds.api.platform.controller;

import com.google.common.collect.Lists;
import com.kay.clouds.api.platform.controller.GetStockListApi.Request;
import com.kay.clouds.api.platform.controller.GetStockListApiControllerTest.Config;
import com.kay.clouds.api.util.testing.TestServerInterface;
import com.kay.clouds.domain.Relation;
import com.kay.clouds.domain.error.ErrorException;
import com.kay.clouds.domain.platform.index.Index;
import com.kay.clouds.domain.platform.index.stock.IndexContainStockRepository;
import com.kay.clouds.domain.platform.stock.Stock;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import static org.springframework.context.annotation.FilterType.ASSIGNABLE_TYPE;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {Config.class})
public class GetStockListApiControllerTest {

    @Configuration
    @ComponentScan(
            basePackages = {"com.kay.clouds.api.platform.controller"},
            useDefaultFilters = false,
            includeFilters = {
                @ComponentScan.Filter(type = ASSIGNABLE_TYPE, value = GetStockListApiController.class)
            })
    public static class Config implements UnitTestConfiguration {

        public class StockData implements Stock {

            private String ticker;
            private String company;
            private String index;
            private String industry;
            private String countryCode;
            private String sector;
            private Long shareNumber;
            private String market;
            private String companyUrl;
            private String currencyCode;

            public String getTicker() {
                return ticker;
            }

            public void setTicker(String ticker) {
                this.ticker = ticker;
            }

            public String getCompany() {
                return company;
            }

            public void setCompany(String company) {
                this.company = company;
            }

            public String getIndex() {
                return index;
            }

            public void setIndex(String index) {
                this.index = index;
            }

            public String getIndustry() {
                return industry;
            }

            public void setIndustry(String industry) {
                this.industry = industry;
            }

            public String getCountryCode() {
                return countryCode;
            }

            public void setCountryCode(String countryCode) {
                this.countryCode = countryCode;
            }

            public String getSector() {
                return sector;
            }

            public void setSector(String sector) {
                this.sector = sector;
            }

            public Long getShareNumber() {
                return shareNumber;
            }

            public void setShareNumber(Long shareNumber) {
                this.shareNumber = shareNumber;
            }

            public String getMarket() {
                return market;
            }

            public void setMarket(String market) {
                this.market = market;
            }

            public String getCompanyUrl() {
                return companyUrl;
            }

            public void setCompanyUrl(String companyUrl) {
                this.companyUrl = companyUrl;
            }

            @Override
            public String getCurrencyCode() {
                return currencyCode;
            }

            public void setCurrencyCode(String currencyCode) {
                this.currencyCode = currencyCode;
            }

        }

        @Override
        public IndexContainStockRepository indexContainStockRepository() {
            return new IndexContainStockRepository() {
                @Override
                public void create(String fromKey, String toKey) throws ErrorException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
                
                @Override
                public void delete(String fromKey, String toKey) throws ErrorException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public Relation<Index, Stock> one(String fromKey, String toKey) throws ErrorException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public List<? extends Relation<Index, Stock>> all(String fromKey) throws ErrorException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public List<? extends Stock> allTo(String fromKey) throws ErrorException {

                    StockData data = new StockData();
                    data.setCompany("campany");
                    data.setCountryCode("country");
                    data.setIndex("index");
                    data.setIndustry("industry");
                    data.setShareNumber(8l);
                    data.setTicker("ticker");
                    List<StockData> dataList = Lists.newArrayList(data);
                    return dataList;

                }
            };

        }
    }

    private TestServerInterface server;

    @Autowired
    GetStockListApiController controller;

    String url;

    @Before
    public void setUp() {
        url = GetStockListApi.URL;
        server = new TestServerInterface(controller);
    }

    @Test
    public void requestTest() throws Exception {

        Request request = new Request();
        String response = server.postResponseString(url, request);

    }

}
