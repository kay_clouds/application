package com.kay.clouds.api.platform.controller;

import com.kay.clouds.api.platform.controller.CreateUserLikeStockApi.Request;
import com.kay.clouds.api.platform.controller.CreateUserLikeStockApiControllerTest.Config;
import com.kay.clouds.api.util.testing.TestServerInterface;
import com.kay.clouds.domain.Relation;
import com.kay.clouds.domain.error.ErrorException;
import com.kay.clouds.domain.platform.stock.Stock;
import com.kay.clouds.domain.platform.user.User;
import com.kay.clouds.domain.platform.user.stock.UserLikeStockRepository;
import java.util.List;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import static org.springframework.context.annotation.FilterType.ASSIGNABLE_TYPE;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {Config.class})
public class CreateUserLikeStockApiControllerTest {

    @Configuration
    @ComponentScan(
            basePackages = {"com.kay.clouds.api.platform.controller"},
            useDefaultFilters = false,
            includeFilters = {
                @ComponentScan.Filter(type = ASSIGNABLE_TYPE, value = CreateUserLikeStockApiController.class)
            })
    public static class Config implements UnitTestConfiguration {

        @Override
        public UserLikeStockRepository likeRepository() {
            return new UserLikeStockRepository() {
                @Override
                public void create(UUID from, String to) throws ErrorException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
                
                @Override
                public void delete(UUID from, String to) throws ErrorException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public List<? extends Relation<User, Stock>> all(UUID from) throws ErrorException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public List<? extends Stock> allTo(UUID from) throws ErrorException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public Relation<User, Stock> one(UUID fromKey, String toKey) throws ErrorException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

            };
        }
    }

    private TestServerInterface server;

    @Autowired
    CreateUserLikeStockApiController controller;

    String url;

    @Before
    public void setUp() {
        url = CreateUserLikeStockApi.URL;
        server = new TestServerInterface(controller);
    }

    @Test
    public void requestTest() throws Exception {

        Request request = new Request();
        String response = server.postResponseString(url, request);
    }

}
