package com.kay.clouds.api.platform.controller;

/**
 *
 * @author daniel.eguia
 */
public class DeleteUserLikeStockApi {

    public final static String URL = "/api/platform/userLikeStockDelete";

    public static class Request {

        public String ticker;

    }

}
