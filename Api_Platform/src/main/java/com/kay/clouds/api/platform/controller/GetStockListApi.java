package com.kay.clouds.api.platform.controller;

import java.util.Date;

/**
 *
 * @author Lili
 */
public class GetStockListApi {

    public final static String URL = "/api/platform/getStockList";

    public static class Request {

        public String featureId;
        public String indexId;
        public Date from;
        public Date to;

        @Override
        public String toString() {
            return "Request{" + "indexId=" + indexId + ", from=" + from + ", to=" + to + '}';
        }

    }

}
