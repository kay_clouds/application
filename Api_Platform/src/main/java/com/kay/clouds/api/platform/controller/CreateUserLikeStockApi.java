package com.kay.clouds.api.platform.controller;

/**
 *
 * @author daniel.eguia
 */
public class CreateUserLikeStockApi {

    public final static String URL = "/api/platform/userLikeStock";

    public static class Request {

        public String ticker;

    }

}
