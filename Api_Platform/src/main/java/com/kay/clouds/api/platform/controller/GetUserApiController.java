package com.kay.clouds.api.platform.controller;

import static com.kay.clouds.api.platform.controller.GetUserApi.URL;
import com.kay.clouds.api.util.handling.ApiAuthenticator;
import com.kay.clouds.api.util.handling.ApiGet;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.error.ErrorCode;
import com.kay.clouds.domain.error.Nulls;
import com.kay.clouds.domain.platform.user.User;
import com.kay.clouds.domain.platform.user.UserRepository;
import com.kay.clouds.domain.security.Permission;
import com.kay.clouds.domain.security.credential.Credential;
import com.kay.clouds.domain.security.credential.CredentialService;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author daniel.eguia
 */
@Controller
@RequestMapping(URL)
public class GetUserApiController extends ApiGet {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CredentialService credentialService;

    public GetUserApiController() {
        //withou session
        withoutSession = Boolean.TRUE;
    }

    @Override
    protected Response get() {

        UUID token = ApiAuthenticator.getToken();
        Credential credential = credentialService.verify(token, Permission.PLATFORM);
        User user = userRepository.one(credential.getId());
        Nulls.requireNonNull(user, ErrorCode.NOT_FOUND_USER);
        return ok(user);

    }
}
