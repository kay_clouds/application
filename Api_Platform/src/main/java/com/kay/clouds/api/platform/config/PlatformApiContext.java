package com.kay.clouds.api.platform.config;

import com.kay.clouds.api.util.config.ApiJsonContext;
import com.kay.clouds.api.util.handling.ApiAuthenticator;
import com.kay.clouds.domain.error.Emptys;
import com.kay.clouds.domain.platform.index.stock.IndexContainStockRepository;
import com.kay.clouds.domain.platform.market.MarketRepository;
import com.kay.clouds.domain.platform.market.index.MarketContainIndexRepository;
import com.kay.clouds.domain.platform.stock.StockRepository;
import com.kay.clouds.domain.platform.user.UserRepository;
import com.kay.clouds.domain.platform.user.stock.UserFollowStockRepository;
import com.kay.clouds.domain.platform.user.stock.UserLikeStockRepository;
import com.kay.clouds.domain.security.Permission;
import com.kay.clouds.domain.security.credential.CredentialRepository;
import com.kay.clouds.domain.security.credential.CredentialService;
import com.kay.clouds.domain.security.token.TokenRepository;
import com.kay.clouds.persistence.platform.PlatformDataBaseHelper;
import com.kay.clouds.persistence.platform.index.stock.contain.CassandraIndexContainStockRepository;
import com.kay.clouds.persistence.platform.market.CassandraMarketRepository;
import com.kay.clouds.persistence.platform.market.index.contain.CassandraMarketContainIndexRepository;
import com.kay.clouds.persistence.platform.stock.CassandraStockRepository;
import com.kay.clouds.persistence.platform.user.CassandraUserRepository;
import com.kay.clouds.persistence.platform.user.stock.follow.CassandraUserFollowStockRepository;
import com.kay.clouds.persistence.platform.user.stock.like.CassandraUserLikeStockRepository;
import com.kay.clouds.persistence.security.CassandraCredentialRepository;
import com.kay.clouds.persistence.security.CassandraSecurityDataBaseHelper;
import com.kay.clouds.persistence.security.CassandraTokenRepository;
import com.kay.clouds.security.credential.UserCredentialService;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { /**
     * controller component
     */
    "com.kay.clouds.api.platform.controller"
})
public class PlatformApiContext extends ApiJsonContext {

    @Bean
    public StockRepository stockRepository() {

        PlatformDataBaseHelper helper = PlatformDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraStockRepository(helper);
    }

    @Bean
    public UserRepository userRepository() {

        PlatformDataBaseHelper helper = PlatformDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraUserRepository(helper);
    }

    @Bean
    public MarketRepository marketRepository() {

        PlatformDataBaseHelper helper = PlatformDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraMarketRepository(helper);
    }

    @Bean
    public CredentialRepository credentialRepository() {

        CassandraSecurityDataBaseHelper helper = CassandraSecurityDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraCredentialRepository(helper);
    }

    @Bean
    public UserFollowStockRepository followRepository() {

        PlatformDataBaseHelper helper = PlatformDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraUserFollowStockRepository(helper);
    }

    @Bean
    public UserLikeStockRepository likeRepository() {

        PlatformDataBaseHelper helper = PlatformDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraUserLikeStockRepository(helper);
    }

    @Bean
    public ApiAuthenticator apiAuthenticator() throws IOException {
        return new ApiAuthenticator() {

            @Autowired
            TokenRepository tokenRepository;

            @Override
            protected Boolean verify(String token) {

                if (Emptys.isNullOrEmpty(token)) {
                    return Boolean.FALSE;
                }

                UUID tokenId = UUID.fromString(token);
                return !Objects.isNull(tokenRepository.exist(tokenId, Permission.PLATFORM));
            }
        };
    }

    @Bean
    public TokenRepository tokenRepository() {

        CassandraSecurityDataBaseHelper helper = CassandraSecurityDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraTokenRepository(helper);
    }

    @Bean
    public CredentialService credentialService() {
        return new UserCredentialService(credentialRepository(), tokenRepository());
    }

    @Bean
    public MarketContainIndexRepository marketContainIndexRepository() {

        PlatformDataBaseHelper helper = PlatformDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraMarketContainIndexRepository(helper);

    }

    @Bean
    public IndexContainStockRepository indexContainStockRepository() {

        PlatformDataBaseHelper helper = PlatformDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraIndexContainStockRepository(helper);

    }

}
