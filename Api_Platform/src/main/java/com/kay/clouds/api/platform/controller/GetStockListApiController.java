package com.kay.clouds.api.platform.controller;

import static com.kay.clouds.api.platform.controller.GetStockListApi.*;
import com.kay.clouds.api.platform.controller.GetStockListApi.Request;
import com.kay.clouds.api.util.handling.ApiPost;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.platform.index.stock.IndexContainStockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Lilie
 */
@Controller
@RequestMapping(URL)
public class GetStockListApiController extends ApiPost<Request> {

    @Autowired
    IndexContainStockRepository stockRepository;

    @Override
    protected Response post(Request request) {
        return ok(stockRepository.allTo(request.indexId));
    }

}
