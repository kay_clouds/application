package com.kay.clouds.api.platform.controller;

/**
 *
 * @author daniel.eguia
 */
public class CreateUserFollowStockApi {

    public final static String URL = "/api/platform/userFollowStock";

    public static class Request {

        public String ticker;
    }

}
