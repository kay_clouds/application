package com.kay.clouds.api.platform.controller;

import com.kay.clouds.api.platform.controller.GetStockApi.Request;
import static com.kay.clouds.api.platform.controller.GetStockApi.URL;
import com.kay.clouds.api.util.handling.ApiPost;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.platform.stock.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Lilie
 */
@Controller
@RequestMapping(URL)
public class GetStockApiController extends ApiPost<Request> {

    @Autowired
    StockRepository stockRepository;

    @Override
    protected Response post(Request request) {
        return ok(stockRepository.one(request.ticker));
    }

}
