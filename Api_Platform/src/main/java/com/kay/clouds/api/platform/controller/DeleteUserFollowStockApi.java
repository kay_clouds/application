package com.kay.clouds.api.platform.controller;

/**
 *
 * @author daniel.eguia
 */
public class DeleteUserFollowStockApi {

    public final static String URL = "/api/platform/userFollowStockDelete";

    public static class Request {

        public String ticker;
    }

}
