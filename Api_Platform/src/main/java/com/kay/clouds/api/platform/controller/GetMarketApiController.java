package com.kay.clouds.api.platform.controller;

import com.kay.clouds.api.platform.controller.GetMarketApi.Data;
import com.kay.clouds.api.platform.controller.GetMarketApi.Request;
import static com.kay.clouds.api.platform.controller.GetMarketApi.URL;
import com.kay.clouds.api.util.handling.ApiPost;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.platform.market.MarketRepository;
import com.kay.clouds.domain.platform.market.index.MarketIndexRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Lilie
 */
@Controller
@RequestMapping(URL)
public class GetMarketApiController extends ApiPost<Request> {

    @Autowired
    MarketIndexRepository marketIndexRepository;

    @Autowired
    MarketRepository marketRepository;

    @Override
    protected Response post(Request request) {
        Data data = new Data();
        data.indexList = marketIndexRepository.allTo(request.marketId);
        data.market = marketRepository.one(request.marketId);
        return ok(data);
    }

}
