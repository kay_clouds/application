package com.kay.clouds.api.platform.controller;

import static com.kay.clouds.api.platform.controller.ExistsUserFollowStockApi.URL;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kay.clouds.api.platform.controller.ExistsUserFollowStockApi.Request;
import com.kay.clouds.api.util.handling.ApiAuthenticator;
import com.kay.clouds.api.util.handling.ApiPost;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.error.ErrorCode;
import com.kay.clouds.domain.error.Nulls;
import com.kay.clouds.domain.platform.user.stock.UserFollowStockRepository;
import com.kay.clouds.domain.security.Permission;
import com.kay.clouds.domain.security.credential.CredentialService;

/**
 *
 * @author daniel.eguia
 */
@Controller
@RequestMapping(value = URL)
public class ExistsUserFollowStockApiController extends ApiPost<Request> {

    @Autowired
    UserFollowStockRepository followRepository;

    @Autowired
    CredentialService credentialService;

    public ExistsUserFollowStockApiController() {

    }

    @Override
    protected Response post(Request request) {
        Nulls.requireNonNull(request.ticker, ErrorCode.INVALID_TICKER);
        UUID credentialId = ApiAuthenticator.getCredentialId(credentialService, Permission.PLATFORM);
        Nulls.requireNonNull(credentialId, ErrorCode.INVALID_CREDENTIAL);

        followRepository.one(credentialId, request.ticker);

        return ok();

    }
}
