package com.kay.clouds.api.platform.controller;

/**
 *
 * @author Lili
 */
public class GetStockApi {

    public final static String URL = "/api/platform/getIndexStock";

    public static class Request {

        public String ticker;

    }

}
