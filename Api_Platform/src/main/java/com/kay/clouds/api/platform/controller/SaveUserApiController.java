package com.kay.clouds.api.platform.controller;

import com.kay.clouds.api.platform.controller.SaveUserApi.Request;
import com.kay.clouds.api.platform.controller.SaveUserApi.SimpleUser;
import static com.kay.clouds.api.platform.controller.SaveUserApi.URL;
import com.kay.clouds.api.util.handling.ApiAuthenticator;
import com.kay.clouds.api.util.handling.ApiPost;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.platform.user.UserRepository;
import com.kay.clouds.domain.security.Permission;
import com.kay.clouds.domain.security.credential.CredentialService;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author daniel.eguia
 */
@Controller
@RequestMapping(value = URL)
public class SaveUserApiController extends ApiPost<Request> {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CredentialService credentialService;

    public SaveUserApiController() {

    }

    @Override
    protected Response post(Request request) {

        UUID credentialId = ApiAuthenticator.getCredentialId(credentialService, Permission.PLATFORM);

        SimpleUser user = new SimpleUser();
        user.setId(credentialId);
        user.nickName = request.nickName;
        userRepository.save(user);
        return ok();

    }
}
