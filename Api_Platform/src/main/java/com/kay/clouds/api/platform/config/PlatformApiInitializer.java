package com.kay.clouds.api.platform.config;

import com.kay.clouds.api.util.config.ApiInitializer;

/**
 * Spring MVC project, and start up configuration. It configure all requirements
 * for java servlet 3.0 .
 *
 * @author Lili
 */
public class PlatformApiInitializer extends ApiInitializer {

    @Override
    protected Class<?> getApplicationContext() {
        return PlatformApiContext.class; //To change body of generated methods, choose Tools | Templates.
    }

}
