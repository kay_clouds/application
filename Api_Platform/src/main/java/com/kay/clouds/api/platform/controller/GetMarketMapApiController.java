package com.kay.clouds.api.platform.controller;

import static com.kay.clouds.api.platform.controller.GetMarketMapApi.*;
import com.kay.clouds.api.util.handling.ApiGet;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.platform.market.Market;
import com.kay.clouds.domain.platform.market.MarketRepository;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Lilie
 */
@Controller
@RequestMapping(URL)
public class GetMarketMapApiController extends ApiGet {

    @Autowired
    MarketRepository marketRepository;

    @Override
    protected Response get() {
        Map<String, List<Market>> marketMap = marketRepository
                .all()
                .stream()
                .collect(Collectors.groupingBy(Market::getCountryCode));
        return ok(marketMap);
    }
}
