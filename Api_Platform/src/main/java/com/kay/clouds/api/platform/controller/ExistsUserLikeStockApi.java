package com.kay.clouds.api.platform.controller;

/**
 *
 * @author daniel.eguia
 */
public class ExistsUserLikeStockApi {

    public final static String URL = "/api/platform/userLikeStockExists";

    public static class Request {

        public String ticker;

    }

}
