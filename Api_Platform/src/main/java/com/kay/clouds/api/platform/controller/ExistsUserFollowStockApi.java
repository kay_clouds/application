package com.kay.clouds.api.platform.controller;

/**
 *
 * @author daniel.eguia
 */
public class ExistsUserFollowStockApi {

    public final static String URL = "/api/platform/userFollowStockExists";

    public static class Request {

        public String ticker;
    }

}
