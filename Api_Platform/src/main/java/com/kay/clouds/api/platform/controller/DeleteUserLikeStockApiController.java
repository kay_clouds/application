package com.kay.clouds.api.platform.controller;

import com.kay.clouds.api.platform.controller.DeleteUserLikeStockApi.Request;
import static com.kay.clouds.api.platform.controller.DeleteUserLikeStockApi.URL;
import com.kay.clouds.api.util.handling.ApiAuthenticator;
import com.kay.clouds.api.util.handling.ApiPost;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.error.ErrorCode;
import com.kay.clouds.domain.error.Nulls;
import com.kay.clouds.domain.security.Permission;
import com.kay.clouds.domain.security.credential.CredentialService;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.kay.clouds.domain.platform.user.stock.UserLikeStockRepository;

/**
 *
 * @author daniel.eguia
 */
@Controller
@RequestMapping(value = URL)
public class DeleteUserLikeStockApiController extends ApiPost<Request> {

    @Autowired
    UserLikeStockRepository likeRepository;

    @Autowired
    CredentialService credentialService;

    public DeleteUserLikeStockApiController() {

    }

    @Override
    protected Response post(Request request) {

        Nulls.requireNonNull(request.ticker, ErrorCode.INVALID_TICKER);
        UUID credentialId = ApiAuthenticator.getCredentialId(credentialService, Permission.PLATFORM);

        Nulls.requireNonNull(credentialId, ErrorCode.INVALID_CREDENTIAL);
        likeRepository.delete(credentialId, request.ticker);
        return ok();
    }
}
