package com.kay.clouds.api.platform.controller;

import com.kay.clouds.domain.analytics.Feature;
import com.kay.clouds.domain.platform.index.Index;
import com.kay.clouds.domain.platform.market.Market;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Lili
 */
public class GetMarketApi {

    public final static String URL = "/api/platform/getMarket";

    public static class Request {

        public String marketId;
    }

    public static class Data {

        public Market market;
        public List<Feature> featureList = Arrays.asList(Feature.values());
        public List<? extends Index> indexList;
    }

}
