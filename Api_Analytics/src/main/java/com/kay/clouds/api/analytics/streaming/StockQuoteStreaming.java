package com.kay.clouds.api.analytics.streaming;

import com.kay.clouds.analytics.quote.yahoo.QuoteServiceImpl;
import com.kay.clouds.domain.analytics.quote.Quote;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;

/**
 *
 * @author lili
 */
//@Controller
public class StockQuoteStreaming extends QuoteServiceImpl {

    private final SimpMessagingTemplate template;
    private final List<String> tickerList;

    @Autowired
    public StockQuoteStreaming(SimpMessagingTemplate template) {
        super();
        this.template = template;
        tickerList = new ArrayList();
        tickerList.add("ABE.MC");
        tickerList.add("ANA.MC");
        tickerList.add("ACX.MC");
        tickerList.add("ACS.MC");
        tickerList.add("AENA.MC");
        tickerList.add("AMS.MC");
        tickerList.add("MTS.MC");
        tickerList.add("SAB.MC");
        tickerList.add("POP.MC");
        tickerList.add("BKIA.MC");
        tickerList.add("BKT.MC");
        tickerList.add("CABK.MC");
        tickerList.add("DIA.MC");
        tickerList.add("ENG.MC");
        tickerList.add("ELE.MC");
        tickerList.add("FCC.MC");
        tickerList.add("FER.MC");
        tickerList.add("GAM.MC");
        tickerList.add("GAS.MC");
        tickerList.add("GRF.MC");
        tickerList.add("IDR.MC");
        tickerList.add("MAP.MC");
        tickerList.add("TL5.MC");
        tickerList.add("MRL.MC");
        tickerList.add("OHL.MC");
        tickerList.add("REE.MC");
        tickerList.add("REP.MC");
        tickerList.add("SCYR.MC");
        tickerList.add("TRE.MC");
        tickerList.add("AI.PA");
        tickerList.add("AIR.PA");
        tickerList.add("ALV.DE");
        tickerList.add("ITK.BE");
        tickerList.add("ASML.AS");
        tickerList.add("G.MI");
        tickerList.add("CS.PA");
        tickerList.add("BBVA.MC");
        tickerList.add("SAN.MC");
        tickerList.add("BAS.DE");
        tickerList.add("BAYN.DE");
        tickerList.add("BMW.DE");
        tickerList.add("BNP.PA");
        tickerList.add("CA.PA");
        tickerList.add("SGO.PA");
        tickerList.add("DAI.DE");
        tickerList.add("BN.PA");
        tickerList.add("DBK.DE");
        tickerList.add("DPW.DE");
        tickerList.add("DTE.DE");
        tickerList.add("EOAN.DE");
        tickerList.add("ENEL.MI");
        tickerList.add("ENGI.PA");
        tickerList.add("ENI.MI");
        tickerList.add("EI.PA");
        tickerList.add("FRE.DE");
        tickerList.add("IBE.MC");
        tickerList.add("ITX.MC");
        tickerList.add("INGA.AS");
        tickerList.add("ISP.MI");
        tickerList.add("IAG.L");
        tickerList.add("PHI1.DE");
        tickerList.add("OR.PA");
        tickerList.add("MC.PA");
        tickerList.add("MUV2.D");
        tickerList.add("NOKIA.HE");
        tickerList.add("ORA.PA");
        tickerList.add("SAF.PA");
        tickerList.add("SAN.PA");
        tickerList.add("SAP.DE");
        tickerList.add("SU.PA");
        tickerList.add("SIE.DE");
        tickerList.add("GLE.PA");
        tickerList.add("TEF.MC");
        tickerList.add("FP.PA");
        tickerList.add("UL.PA");
        tickerList.add("UCG.MI");
        tickerList.add("UNA.AS");
        tickerList.add("DG.PA");
        tickerList.add("VIV.PA");
        tickerList.add("VOW3.DE");
        tickerList.add("%5E" + "IBEX");
        tickerList.add("%5E" + "STOXX50E");
    }

    @Scheduled(fixedDelay = 100)
    public void quote() throws Exception {
        tickerList.parallelStream().forEach(this::scrapping);
    }

    @Override
    protected void save(Quote quote) {
        template.convertAndSend("/quote/" + quote.getTicker(), quote);
    }

}
