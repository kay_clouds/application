package com.kay.clouds.api.analytics.streaming;

import com.kay.clouds.domain.security.Permission;
import com.kay.clouds.domain.security.token.TokenRepository;
import java.util.Objects;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
//
//@Configuration
//@EnableWebSocketMessageBroker

public class WebSocketBroker extends AbstractWebSocketMessageBrokerConfigurer {

    @Autowired
    TokenRepository tokenRepository;

    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.setInterceptors(new ChannelInterceptorAdapter() {

            @Override
            public Message<?> preSend(Message<?> message, MessageChannel channel) {

                StompHeaderAccessor accessor
                        = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

                if (StompCommand.CONNECT.equals(accessor.getCommand())) {
                    UUID token = UUID.fromString(accessor.getNativeHeader("authorization").get(0));
                    if (Objects.isNull(token) || Objects.isNull(tokenRepository.exist(token, Permission.ANALYTICS))) {
                        StompHeaderAccessor headerAccessor = StompHeaderAccessor.create(StompCommand.ERROR);
                        headerAccessor.setMessage("ACCESS_TOKEN_INVALID");
                        return MessageBuilder.createMessage(new byte[0], headerAccessor.getMessageHeaders());
                    }
                }

                return message;
            }
        });
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/quote");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/api/analytics/streaming").setAllowedOrigins("*").withSockJS();
    }

}
