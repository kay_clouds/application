package com.kay.clouds.api.analytics.controller;

import com.kay.clouds.analytics.web.WebMetaServiceImpl;
import com.kay.clouds.api.analytics.controller.GetWebMetaApi.RequestMeta;
import com.kay.clouds.api.util.handling.ApiPost;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.analytics.exchange.StockExchangeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Lilie
 */
@Controller
@RequestMapping(GetWebMetaApi.URL)
public class GetWebMetaApiController extends ApiPost<RequestMeta> {

    @Autowired
    StockExchangeRepository repository;

    @Override
    public Response post(RequestMeta request) {

        WebMetaServiceImpl service = new WebMetaServiceImpl() {
        };
        return ok(service.scrapping(request.webUrl));
    }
}
