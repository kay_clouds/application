package com.kay.clouds.api.analytics.config;

import com.kay.clouds.analytics.feature.ExchangeFeatureService;
import com.kay.clouds.api.util.config.ApiJsonContext;
import com.kay.clouds.api.util.handling.ApiAuthenticator;
import com.kay.clouds.domain.analytics.exchange.IndexExchangeRepository;
import com.kay.clouds.domain.analytics.exchange.StockExchangeRepository;
import com.kay.clouds.domain.error.Emptys;
import com.kay.clouds.domain.security.Permission;
import com.kay.clouds.domain.security.credential.CredentialRepository;
import com.kay.clouds.domain.security.credential.CredentialService;
import com.kay.clouds.domain.security.token.TokenRepository;
import com.kay.clouds.persistence.analytics.DataWarehouseHelper;
import com.kay.clouds.persistence.analytics.exchange.index.CassandraIndexExchangeRepository;
import com.kay.clouds.persistence.analytics.exchange.stock.CassandraStockExchangeRepository;
import com.kay.clouds.persistence.security.CassandraCredentialRepository;
import com.kay.clouds.persistence.security.CassandraSecurityDataBaseHelper;
import com.kay.clouds.persistence.security.CassandraTokenRepository;
import com.kay.clouds.security.credential.UserCredentialService;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@ComponentScan(basePackages = { /**
     * controller component
     */
    "com.kay.clouds.api.analytics.controller",
    "com.kay.clouds.api.analytics.streaming"
})
public class AnalyticsApiContext extends ApiJsonContext {

    @Bean
    public TokenRepository tokenRepository() {

        CassandraSecurityDataBaseHelper helper = CassandraSecurityDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraTokenRepository(helper);
    }

    @Bean
    public CredentialRepository credentialRepository() {

        CassandraSecurityDataBaseHelper helper = CassandraSecurityDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraCredentialRepository(helper);
    }

    @Bean
    public CredentialService credentialService() {
        return new UserCredentialService(credentialRepository(), tokenRepository());
    }

    @Bean
    public ApiAuthenticator apiAuthenticator() throws IOException {
        return new ApiAuthenticator() {

            @Autowired
            TokenRepository repository;

            @Override
            protected Boolean verify(String token) {

                if (Emptys.isNullOrEmpty(token)) {
                    return Boolean.FALSE;
                }

                UUID tokenId = UUID.fromString(token);
                return !Objects.isNull(repository.exist(tokenId, Permission.PLATFORM));
            }
        };
    }

    @Bean
    public StockExchangeRepository stockExchangeRepository() {

        DataWarehouseHelper helper = DataWarehouseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraStockExchangeRepository(helper);
    }

    @Bean
    public IndexExchangeRepository indexExchangeRepository() {

        DataWarehouseHelper helper = DataWarehouseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraIndexExchangeRepository(helper);
    }

    @Bean
    public ExchangeFeatureService featureService(StockExchangeRepository repository) {
        return new ExchangeFeatureService(repository);
    }

}
