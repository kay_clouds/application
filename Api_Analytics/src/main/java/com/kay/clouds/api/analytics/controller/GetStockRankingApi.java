package com.kay.clouds.api.analytics.controller;

import com.kay.clouds.domain.analytics.Feature;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lili
 */
public class GetStockRankingApi {

    public final static String URL = "/api/analytics/getStockRanking";

    public static class Request {

        public List<String> stockTickerList;
        public Feature feature;
        public Date from;
        public Date to;

    }

}
