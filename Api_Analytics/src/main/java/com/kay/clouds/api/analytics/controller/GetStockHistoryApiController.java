package com.kay.clouds.api.analytics.controller;

import com.kay.clouds.api.analytics.controller.GetStockHistoryApi.Request;
import com.kay.clouds.api.util.handling.ApiPost;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.analytics.exchange.StockExchangeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Lilie
 */
@Controller
@RequestMapping(GetStockHistoryApi.URL)
public class GetStockHistoryApiController extends ApiPost<Request> {

    @Autowired
    StockExchangeRepository repository;

    @Override
    public Response post(Request request) {

        return ok(repository.getAscendantListBetween(
                request.stockTicker,
                request.from,
                request.to
        ));

    }
}
