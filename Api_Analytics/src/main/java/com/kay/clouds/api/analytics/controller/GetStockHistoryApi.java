package com.kay.clouds.api.analytics.controller;

import java.util.Date;

/**
 *
 * @author Lili
 */
public class GetStockHistoryApi {

    public final static String URL = "/api/analytics/getStockHistory";

    public static class Request {

        public String stockTicker;
        public Date from;
        public Date to;

        @Override
        public String toString() {
            return "Request{" + "stockTicker=" + stockTicker + ", from=" + from + ", to=" + to + '}';
        }

    }

}
