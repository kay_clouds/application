package com.kay.clouds.api.analytics.controller;

import com.kay.clouds.analytics.feature.ExchangeFeatureService;
import com.kay.clouds.api.analytics.controller.GetStockRankingApi.Request;
import com.kay.clouds.api.util.handling.ApiPost;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Lilie
 */
@Controller
@RequestMapping(GetStockRankingApi.URL)
public class GetStockRankingApiController extends ApiPost<Request> {

    @Autowired
    ExchangeFeatureService featureService;

    @Override
    public Response post(Request request) {

        return ok(featureService.calculate(
                request.feature,
                request.stockTickerList,
                request.from,
                request.to
        ));

    }
}
