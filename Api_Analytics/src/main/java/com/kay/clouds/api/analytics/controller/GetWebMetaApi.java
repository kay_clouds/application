package com.kay.clouds.api.analytics.controller;

/**
 *
 * @author daniel.eguia
 */
public class GetWebMetaApi {

    public final static String URL = "/api/analytics/getWebMeta";

    public static class RequestMeta {

        public String webUrl;

        @Override
        public String toString() {
            return "Request{" + "webUrl=" + webUrl + '}';
        }

    }

}
