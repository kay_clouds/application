package com.kay.clouds.api.analytics.controller;

import com.google.common.collect.Lists;
import com.kay.clouds.api.analytics.controller.GetStockHistoryApi.Request;
import com.kay.clouds.api.analytics.controller.GetStockHistoryApiControllerTest.Config;
import com.kay.clouds.api.util.handling.ApiResponse;
import com.kay.clouds.api.util.testing.TestServerInterface;
import com.kay.clouds.domain.analytics.exchange.Exchange;
import com.kay.clouds.domain.analytics.exchange.StockExchangeRepository;
import java.util.Date;
import java.util.List;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import static org.springframework.context.annotation.FilterType.ASSIGNABLE_TYPE;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {Config.class})
public class GetStockHistoryApiControllerTest {

    private static final String testValue = "Test byte array";

    @Configuration
    @ComponentScan(
            basePackages = {"com.kay.clouds.api.analytics.controller"},
            useDefaultFilters = false,
            includeFilters = {
                @ComponentScan.Filter(type = ASSIGNABLE_TYPE, value = GetStockHistoryApiController.class)
            })
    public static class Config implements UnitTestConfiguration {

        @Bean
        public StockExchangeRepository stockExchangeRepository() {

            return new StockExchangeRepository() {
                @Override
                public void save(Exchange stockExchange) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public List<? extends Exchange> getDescendantListBelow(String stockTicker, Date date, Integer limit) {
                    return Lists.newArrayList();
                }

                @Override
                public List<? extends Exchange> getAscendantListBetween(String key, Date starting, Date ending) {
                    return Lists.newArrayList();//To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public List<? extends Exchange> all() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public Exchange one(String key) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public List<? extends Exchange> in(List<String> keys) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            };
        }
    }

    private TestServerInterface server;

    @Autowired
    GetStockHistoryApiController controller;

    String url;

    @Before
    public void setUp() {
        url = GetStockHistoryApi.URL;
        server = new TestServerInterface(controller);
    }

    @Test
    public void requestTest() throws Exception {

        Request request = new Request();
        request.from = new Date();
        request.to = new Date();
        request.stockTicker = "stockTicker";

        ApiResponse<List> response = server.postResponse(url, request, List.class);
        assertThat(response.getResponseData()).isNotNull();

    }

}
