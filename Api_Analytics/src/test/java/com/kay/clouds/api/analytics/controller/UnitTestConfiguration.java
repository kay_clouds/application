package com.kay.clouds.api.analytics.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kay.clouds.analytics.exchange.yahoo.ExchangeServiceImpl;
import com.kay.clouds.api.util.handling.ApiAuthenticator;
import com.kay.clouds.api.util.handling.ApiLogger;
import com.kay.clouds.domain.security.credential.CredentialRepository;
import com.kay.clouds.domain.security.credential.CredentialService;
import java.io.IOException;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;

/**
 *
 * @author Lili
 */
public interface UnitTestConfiguration {

    @Bean
    default ObjectMapper getObjectMapper() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        return mapper;
    }

    @Bean
    default ApiAuthenticator apiAuthenticator() throws IOException {
        return new ApiAuthenticator() {
            @Override
            protected Boolean verify(String token) {
                return Boolean.TRUE;
            }
        };
    }

    @Bean
    default Logger getLogger() throws IOException {
        return ApiLogger.LOGGER;
    }

    @Bean
    default ExchangeServiceImpl getEtractionService() {
        return null;
    }

    @Bean
    default CredentialRepository credentialRepository() {

        return null;
    }

    @Bean
    default CredentialService credentialService() {
        return null;
    }

}
