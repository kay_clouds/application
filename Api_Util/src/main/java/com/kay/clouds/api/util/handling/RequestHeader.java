package com.kay.clouds.api.util.handling;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Lili
 */
public interface RequestHeader {

    public static final String ACCESSTOKEN_HEADER_NAME = "accessToken";
    public static final String API_VERSION = "version";

    public static String extractToken(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getHeader(ACCESSTOKEN_HEADER_NAME);
    }

    public static String extractVersion(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getHeader(API_VERSION);
    }

}
