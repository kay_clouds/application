package com.kay.clouds.api.util.handling;

import com.kay.clouds.domain.error.ErrorException;
import java.util.List;
import java.util.function.Supplier;

/**
 *
 * @author Lili
 */
public interface Response {

    public static Response resolve(Supplier<Response> supplier) {
        try {
            return supplier.get();
        } catch (ErrorException errorException) {
            return ApiResponse.error(errorException.getError());
        }
    }

    enum Status {
        REQUEST_ERROR, OK, SERVER_ERROR
    }

    Status getStatus();

    <RESPONSEDATA> RESPONSEDATA getResponseData();

    List<String> getErrorList();

}
