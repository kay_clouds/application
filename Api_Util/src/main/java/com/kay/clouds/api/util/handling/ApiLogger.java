package com.kay.clouds.api.util.handling;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kay.clouds.api.util.logging.LogEntry;
import com.kay.clouds.api.util.logging.LogFactory;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Logger;

/**
 * A java Logger configuration class.
 *
 * @author Lili
 */
public class ApiLogger {

    /**
     * Simple logger.
     */
    public static final Logger LOGGER;

    private static final ObjectMapper MAPPER;

    public ApiLogger() {
    }

    static {
        MAPPER = new ObjectMapper();
        /**
         * Get logger instance.
         */
        LogFactory logFactory = new LogFactory() {
        };
        logFactory.setAppName("Kay_Clouds_Api");
        logFactory.setLoggerName("Kay_Clouds_Api");
        logFactory.setToCassandra(false);
        logFactory.setToConsole(true);
        logFactory.setBasicLoggerConfig(true);
        logFactory.setToFile(false);
        LOGGER = logFactory.getLogger();
    }

    public static void log(HttpServletRequest httpServletRequest, Object request, Object response)
            throws IOException {

        String path = "[" + httpServletRequest.getRequestURI() + "]";
        ApiLogger.LOGGER.info(path);

        // We create the log4j entry
        LogEntry logEntry = new LogEntry(httpServletRequest, ApiLogger.LOGGER);

        // Get headers
        String accessToken = RequestHeader.extractToken(httpServletRequest);
        String apiVersion = RequestHeader.extractVersion(httpServletRequest);

        // Log headers
        logEntry.append("USER_ACCESS_TOKEN   : " + accessToken);
        logEntry.append("API_VERSION   : " + apiVersion);

        // Log body
        logEntry.append("REQUEST BODY  : " + MAPPER.writeValueAsString(request));

        // Log response body
        logEntry.append("RESPONSE BODY : " + MAPPER.writeValueAsString(response));

        // Write log entry
        logEntry.log();
    }
}
