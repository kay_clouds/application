package com.kay.clouds.api.security.controller;

/**
 *
 * @author Lili
 */
public class LoginApi {

    public final static String URL = "/api/security/login";

    public static class Request {

        private String email;
        private String password;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

    }

}
