package com.kay.clouds.api.security.controller;

/**
 *
 * @author Lili
 */
public class CreateCredentialApi {

    public final static String URL = "/api/security/createCredential";

    public static class Request {

        private String email;
        private String password;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

    }

}
