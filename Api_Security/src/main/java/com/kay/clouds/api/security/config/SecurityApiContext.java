package com.kay.clouds.api.security.config;

import com.kay.clouds.api.util.config.ApiJsonContext;
import com.kay.clouds.api.util.handling.ApiAuthenticator;
import com.kay.clouds.domain.security.credential.CredentialRepository;
import com.kay.clouds.domain.security.credential.CredentialService;
import com.kay.clouds.domain.security.token.TokenRepository;
import com.kay.clouds.persistence.security.CassandraCredentialRepository;
import com.kay.clouds.persistence.security.CassandraSecurityDataBaseHelper;
import com.kay.clouds.persistence.security.CassandraTokenRepository;
import com.kay.clouds.security.credential.UserCredentialService;
import java.io.IOException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { /**
     * controller component
     */
    "com.kay.clouds.api.security.controller"
})
public class SecurityApiContext extends ApiJsonContext {

    @Bean
    public CredentialRepository credentialRepository() {

        CassandraSecurityDataBaseHelper helper = CassandraSecurityDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraCredentialRepository(helper);
    }
    
    
    @Bean
    public TokenRepository tokenRepository() {

        CassandraSecurityDataBaseHelper helper = CassandraSecurityDataBaseHelper
                .getCassandraDataSourceHelper("localhost");

        return new CassandraTokenRepository(helper);
    }

    @Bean
    public CredentialService credentialService() {
        return new UserCredentialService(credentialRepository(), tokenRepository());
    }

    @Bean
    public ApiAuthenticator apiAuthenticator() throws IOException {
        return new ApiAuthenticator() {

            @Override
            protected Boolean verify(String token) {

                return Boolean.TRUE;
            }
        };
    }

}
