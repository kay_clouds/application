package com.kay.clouds.api.security.controller;

/**
 *
 * @author Obaib
 */
public class ExistCredentialApi {

    public final static String URL = "/api/security/existCredential";

    public static class Request {

        private String email;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

}
