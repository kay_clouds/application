package com.kay.clouds.api.security.controller;

import static com.kay.clouds.api.security.controller.ExistCredentialApi.*;
import com.kay.clouds.api.security.controller.ExistCredentialApi.Request;
import com.kay.clouds.api.util.handling.ApiPost;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.error.Booleans;
import com.kay.clouds.domain.error.ErrorCode;
import com.kay.clouds.domain.security.credential.CredentialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Lili
 */
@Controller
@RequestMapping(URL)
public class ExistCredentialApiController extends ApiPost<Request> {

    @Autowired
    CredentialService credentialService;

    @Override
    protected Response post(Request request) {
        Booleans.requireTrue(credentialService.exist(request.getEmail()), ErrorCode.NOT_FOUND_CREDENTIAL);
        return ok();
    }

}
