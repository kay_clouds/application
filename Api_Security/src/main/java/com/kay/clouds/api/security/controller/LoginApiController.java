package com.kay.clouds.api.security.controller;

import com.kay.clouds.api.security.controller.CreateCredentialApi.Request;
import static com.kay.clouds.api.security.controller.LoginApi.URL;
import com.kay.clouds.api.util.handling.ApiPost;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.error.ErrorCode;
import com.kay.clouds.domain.error.Nulls;
import com.kay.clouds.domain.security.credential.Credential;
import com.kay.clouds.domain.security.credential.CredentialService;
import com.kay.clouds.domain.security.token.TokenRepository;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Lilie
 */
@Controller
@RequestMapping(value = URL)
public class LoginApiController extends ApiPost<Request> {

    @Autowired
    CredentialService credentialService;

    @Autowired
    TokenRepository tokenRepository;

    public LoginApiController() {
        //withou session
        withoutSession = Boolean.TRUE;
    }

    @Override
    protected Response post(Request request) {

        Credential credential = credentialService.verify(request.getEmail(), request.getPassword());
        Nulls.requireNonNull(credential, ErrorCode.INVALID_CREDENTIAL);
        UUID token = tokenRepository.create(credential.getPermissionList(), TokenRepository.ONE_DAY, credential.getId());
        return ok(token);

    }
}
