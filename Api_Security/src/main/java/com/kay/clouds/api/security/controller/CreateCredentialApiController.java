package com.kay.clouds.api.security.controller;

import static com.kay.clouds.api.security.controller.CreateCredentialApi.*;
import com.kay.clouds.api.security.controller.CreateCredentialApi.Request;
import com.kay.clouds.api.util.handling.ApiPost;
import static com.kay.clouds.api.util.handling.ApiResponse.ok;
import com.kay.clouds.api.util.handling.Response;
import com.kay.clouds.domain.error.Booleans;
import com.kay.clouds.domain.error.ErrorCode;
import com.kay.clouds.domain.security.credential.CredentialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Lilie
 */
@Controller
@RequestMapping(value = URL)
public class CreateCredentialApiController extends ApiPost<Request> {

    @Autowired
    CredentialService credentialService;

    public CreateCredentialApiController() {
        //withou session
        withoutSession = Boolean.TRUE;
    }

    @Override
    protected Response post(Request request) {

        Booleans.requireFalse(credentialService.exist(request.getEmail()), ErrorCode.EXIST_CREDENTIAL);
        credentialService.createCredential(request.getEmail(), request.getPassword());
        return ok();

    }
}
