package com.kay.clouds.api.security.config;

import com.kay.clouds.api.util.config.ApiInitializer;

/**
 * Spring MVC project, and start up configuration. It configure all requirements
 * for java servlet 3.0 .
 *
 * @author Lili
 */
public class SecurityApiInitializer extends ApiInitializer {

    @Override
    protected Class<?> getApplicationContext() {
        return SecurityApiContext.class; //To change body of generated methods, choose Tools | Templates.
    }

}
