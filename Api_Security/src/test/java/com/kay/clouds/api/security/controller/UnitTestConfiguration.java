package com.kay.clouds.api.security.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kay.clouds.api.util.handling.ApiAuthenticator;
import com.kay.clouds.api.util.handling.ApiLogger;
import com.kay.clouds.domain.platform.stock.StockRepository;
import java.io.IOException;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;

/**
 *
 * @author Lili
 */
public interface UnitTestConfiguration {

    @Bean
    default ApiAuthenticator apiAuthenticator() throws IOException {
        return new ApiAuthenticator() {
            @Override
            protected Boolean verify(String text) {
                return Boolean.TRUE;
            }
        };
    }

    @Bean
    default ObjectMapper getObjectMapper() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        return mapper;
    }

    @Bean
    default Logger getLogger() throws IOException {
        return ApiLogger.LOGGER;
    }

    @Bean
    default StockRepository getStockRepository() {
        return null;
    }

}
