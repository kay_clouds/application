var DataSchema = function () {
    this.x = 'Date';
    this.columns = [['Date'], ['Open Price'], ['Bid Price'], ['Ask Volume'], ['Ask Price']];
    this.axes = {
        'Open Price': 'y2',
        'Bid Price': 'y2',
        'Ask Price': 'y2'
    };
    this.types = {
        'Ask Volume': 'bar' // ADD
    };
};

var axis = {
    x: {
        type: 'timeseries',
        tick: {
            format: '%Y-%m-%d %H:%m:%S',
            rotate: 90
        }
    },
    y: {
        label: {
            text: 'Ask Volume',
            position: 'outer-middle'
        }
    },
    y2: {
        show: true,
        label: {
            text: 'Open Prices',
            position: 'outer-middle'
        },
        tick: {
            format: D3.format('.2f')
        }
    }
};

var StreamingChart = React.createClass({
    displayName: 'chart',

    getInitialState: function getInitialState() {
        return {data: new DataSchema()};
    },

    componentDidMount: function componentDidMount() {
        this.loadDataFromServer();
    },
    loadDataFromServer: function loadDataFromServer() {
        var self = this;

        Analytics.QuoteStreaming.streaming(
                "/quote/" + self.props.ticker,
                function (message) {
                    var messageObject = JSON.parse(message);
                    // response.query.results.quote(function (d) {
                    self.state.data.columns[0].push(new Date(messageObject.date));
                    self.state.data.columns[1].push(messageObject.openPrice);
                    self.state.data.columns[2].push(messageObject.bidPrice);
                    self.state.data.columns[3].push(messageObject.askVolume);
                    self.state.data.columns[4].push(messageObject.askPrice);
                    // });

                    if (self.state.data.columns[0].length > 12) {
                        self.state.data.columns.forEach(function (column){
                            column.splice(1, 1);
                        });
                    }

                    self.setState({
                        data: self.state.data
                    });

                });

    },

    render: function () {
        return <C3Chart data={this.state.data} axis={axis}></C3Chart>;
    }
});

module.exports = StreamingChart;