class StockActionButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        };
        this.loadDataFromServer(props);
    };

    loadDataFromServer(props) {
        props.get.call({
            ticker: props.ticker,
        }).then((data) => this.setState({ activated: true }))
        .catch((e) => this.setState({ activated: false }));
    }

    click(e) {
        if(this.state.activated) {
            this.props.unAction.call({
                ticker: this.props.ticker
            }).then(
                (data) => this.setState({ activated: false })
                );
        }else {
            this.props.action.call({
                ticker: this.props.ticker
            }).then(
                (data) => this.setState({ activated: true })
                );
        }
    };

    render() {
        var Icon = this.props.icon;
        return (           
            <IconButton tooltip={this.props.nameButton} onClick={(e) => this.click(e)}>
                <Icon color={this.state.activated ? this.props.colorActivated : ''}/>
            </IconButton>);
    }
};

module.exports = StockActionButton;