
var DataSchema = function () {
    this.x = 'Date';
    this.columns = [['Date'], [], [], [], [], []];
};

var axis = {
    x: {
        type: 'timeseries',
        tick: {
            format: '%Y-%m-%d',
            rotate: 90
        }
    },
};

var RankingChart = function (props) {

    var dataScheme = new DataSchema();
    //Set headers
    dataScheme.columns[1].push(props.data.stockNames[0]);
    dataScheme.columns[2].push(props.data.stockNames[1]);
    dataScheme.columns[3].push(props.data.stockNames[2]);
    dataScheme.columns[4].push(props.data.stockNames[3]);
    dataScheme.columns[5].push(props.data.stockNames[4]);
    //Set values
    props.data.forEach(
            (data) => {

        dataScheme.columns[0].push(new Date(data.date));
        dataScheme.columns[1].push(data.stocks[0]);
        dataScheme.columns[2].push(data.stocks[1]);
        dataScheme.columns[3].push(data.stocks[2]);
        dataScheme.columns[4].push(data.stocks[3]);
        dataScheme.columns[4].push(data.stocks[4]);
    }
    );
    return <C3Chart data={dataScheme} axis={axis}></C3Chart>;
};

module.exports = RankingChart;