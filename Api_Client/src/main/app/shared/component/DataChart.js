
var DataSchema = function () {
    this.x = 'Date';
    this.columns = [['Date'], ['Low'], ['High'], ['Volume'], ['Closing Price']];
    this.axes = {
        Low: 'y2',
        High: 'y2',
        Price: 'y2',
        'Closing Price': 'y2'
    };
    this.types = {
        Volume: 'bar' // ADD
    };
};

var axis = {
    x: {
        type: 'timeseries',
        tick: {
            format: '%Y-%m-%d',
            rotate: 90
        }
    },
    y: {
        label: {
            text: 'Volume',
            position: 'outer-middle',
        },
        tick: {
            // format: D3.format('.2f')
        }
    },
    y2: {
        show: true,
        label: {
            text: 'High',
            position: 'outer-middle'
        },
        tick: {
            format: D3.format('.2f')
        }
    }
};

var DataChart = function (props) {

    var dataScheme = new DataSchema();
    props.data.forEach(
            (data) => {

        dataScheme.columns[0].push(new Date(data.date));
        dataScheme.columns[1].push(data.dailyMinPrice);
        dataScheme.columns[2].push(data.dailyMaxPrice);
        dataScheme.columns[3].push(data.exchangeVolume);
        dataScheme.columns[4].push(data.closingPrice);
    }
    );
    return <C3Chart data={dataScheme} axis={axis}></C3Chart>;
};

module.exports = DataChart;