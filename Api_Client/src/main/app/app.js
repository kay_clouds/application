React = require('react');
Remarkable = require('remarkable');
ReactDOM = require('react-dom');

require('es6-promise').polyfill();
require('isomorphic-fetch');
require("babel-register");

moment = require('moment');
//new version d3 
D3=require("d3");
C3Chart = require('react-c3js').default;
Datamap=require('datamaps');

//streaming api
SockJS= require('sockjs-client');
Stomp = require('stompjs');

//rest api
Platform = require('./api/Platform');
Analytics = require('./api/Analytics');
Security = require('./api/Security');
Authenticator = require('./api/util/Authenticator');
ApiError = require('./api/Error');

// shared
Urls = require('./shared/Urls');
Page = require('./shared/Page');

// componet
Material = require('./shared/component/Material');
DataChart = require('./shared/component/DataChart');
StreamingChart = require('./shared/component/StreamingChart');
StockActionButton = require('./shared/component/StockActionButton');

//Material UI components commons
Button = Material.Button;
TextField = Material.TextField;
FontIcon = Material.FontIcon;
Card = Material.Card;
CardHeader = Material.CardHeader;
CardText = Material.CardText;
CardActions = Material.CardActions;
IconButton  = Material.IconButton;

