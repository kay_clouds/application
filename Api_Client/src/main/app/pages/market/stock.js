class StockDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      company: {},
      stock: {}
    };

    this.loadDataFromServer(props);
  };

  loadDataFromServer(props) {
    var self = this;

    this.setState({ stock: props.stock })

    Analytics.GetWebMetaApi.call({ webUrl: props.stock.companyUrl })
      .then(function (data) {
        console.log(data);
        self.setState({
          company: data
        });
      }).catch(function (e) {
        console.log(e);
      });
  }

  render() {
    return (
      <div className="container">
        <div className="row paddingcard">
          <div className="col-sm-12">
            <Card>
              <CardText>
                <h2>{this.props.stock.ticker} real time chart</h2>
                <StreamingChart ticker={this.props.stock.ticker} ></StreamingChart>
                <p>{this.props.stock.index}</p>
                <p>{this.props.stock.country}</p>
              </CardText>
            </Card>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6">
            <Card>
              <CardText>
                <div className="stock">
                  <h2>{this.state.company.name}</h2>
                  <img src={this.state.company.imageUrl}></img>
                  <p>{this.state.company.description}</p>
                  <a href={this.state.company.pageUrl} target="_blank">company page</a>
                </div>
              </CardText>
            </Card>
          </div>
          <div className="col-sm-6">
            <Card>
              <CardText>
                <div className="stock">
                  <h1>news</h1>
                  <img src='http://3.bp.blogspot.com/-lHfswZPkKbc/VNpEH9LDruI/AAAAAAAALGg/VGlBYzfLnCc/s1600/cabeceraenvuelta.png'></img>
                  <p> <a href="https://www.google.es/#q=or.pa&*">google</a> </p>
                  <p> <a href="https://finance.yahoo.com/quote/OR.PA?ltr=1">yahoo</a></p>
                  <p> <a href="">facebook</a></p>
                </div>
              </CardText>
            </Card>
          </div>
        </div>
      </div>
    );
  }
};

class Stock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };

    var ticker = Urls.getParameterByName("ticker", document.URL);
    Platform.getIndexStock.call({ ticker: ticker }).then(
      (data) => {
        this.setState({
          stock: data
        });
      });
  };

  render() {
    if (this.state.stock) {
      this.props.title(this.state.stock.company);
      return (
        <StockDetail stock={this.state.stock}> </StockDetail>);
    } else {
      return (<div></div>);
    }
  }
};


window.onload = function () {

  ReactDOM.render(
    <Page menu='1'>
      <Stock> </Stock>
    </Page>, document.getElementById("main"));

};


