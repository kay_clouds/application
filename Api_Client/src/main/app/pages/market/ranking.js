var DatePicker = Material.DatePicker;
var Tabs = Material.Tabs;
var Tab = Material.Tab;
var Toolbar = Material.Toolbar;
var ToolbarTitle = Material.ToolbarTitle;
var ToolbarGroup = Material.ToolbarGroup;
var SelectField = Material.SelectField;
var MenuItem = Material.MenuItem;
var FollowIcon = Material.FollowIcon;
var LikeIcon = Material.LikeIcon;

var IndexItem = React.createClass({
    displayName: "Index",
    getInitialState: function () {
        var self = this;
        Analytics.QuoteStreaming.streaming(
            "/quote/" + this.props.index.ticker,
            function (message) {
                var messageObject = JSON.parse(message);
                var row = { askPrice: messageObject["askPrice"], askVolume: messageObject["askVolume"] };;
                self.setState({ row: row });

            });

        return {
            row: { askPrice: 0.0, askVolume: 0 }
        };
    },

    render: function render() {

        return (
            <div>
                {this.props.index.ticker}
                ask price:{this.state.row.askPrice}
                ask volume:{this.state.row.askVolume}
            </div>);
    }
});

var IndexList = function (props) {

    var indexNodes = props.indexList.map(function (index) {
        return (<Tab key={index.ticker} value={index.ticker} onActive={props.handle} label={index.ticker}>

        </Tab>);
    });

    return (<Tabs>
        {indexNodes}
    </Tabs>);

};

class Analyzer extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.props.filter;
        this.state.featureValue = 0;
        this.state.featureList = this.props.featureList;
    };

    handleChangeFeature(event, index, value) {
        var state = this.state;
        state.featureValue = index;
        state.featureId = state.featureList[value];
        this.setState(state);
    };

    handleChangeFrom(e, date) {
        this.setState({ from: date });
    };

    handleChangeTo(e, date) {
        this.setState({ to: date });
    };

    update(e) {
        this.props.onchange(this.state);
    };

    render() {

        var featureList = this.props.featureList.map(
            function (feature, i) {

                return <MenuItem key={i} value={i} primaryText={feature} />;
            }
        );
        return (
            <Toolbar>

                <ToolbarGroup>
                    <ToolbarTitle text="Feature" />
                    <SelectField value={this.state.featureValue} onChange={(event, index, value) => this.handleChangeFeature(event, index, value)}>
                        {featureList}
                    </SelectField>
                </ToolbarGroup>

                <ToolbarGroup>
                    <ToolbarTitle text="From" />
                    <DatePicker defaultDate={this.state.from}
                        onChange={(e, date) => this.handleChangeFrom(e, date)} hintText="From"
                        container="inline" maxDate={new Date(moment())} >
                    </DatePicker>
                </ToolbarGroup>

                <ToolbarGroup>
                    <ToolbarTitle text="To" />
                    <DatePicker defaultDate={this.state.to} onChange={(e, date) => this.handleChangeTo(e, date)} hintText="To"
                        container="inline" maxDate={new Date(moment())}>
                    </DatePicker>
                </ToolbarGroup>
                <ToolbarGroup>
                    <Button type="submit" label="Update" onClick={(e) => this.update(e)}>
                    </Button>
                </ToolbarGroup>
            </Toolbar>);
    };
};


class StockImage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
        this.loadDatafromServer(props);

    };

    componentWillReceiveProps(nextProps) {
        this.loadDatafromServer(nextProps);
    }

    loadDatafromServer(props) {
        Analytics.GetStockHistoryApi.call({ stockTicker: props.ticker, from: props.from, to: props.to })
            .then((data) => this.setState({ data: data.stockExchangeList }));
    }


    render() {
        return (<DataChart data={this.state.data}></DataChart>);
    }
};

class Stock extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    };

    render() {
        var url = "./stock.html?ticker=" + this.props.ticker;
        return (
            <div className="paddingcard">
                <Card style="height: 500px;">
                    <CardText>
                        <StockImage {...this.props}>
                        </StockImage>

                        <h1 className="stockTicker center" >
                            <a href={url}>{this.props.company}</a>
                        </h1>
                    </CardText>
                    <CardActions>
                        <div className="right">
                            <StockActionButton nameButton={'Like'} ticker={this.props.ticker} get={ Platform.GetLikeStock} unAction={Platform.UnLikeStock} action={Platform.LikeStock} icon={LikeIcon} colorActivated={'#D62728'}/>
                            <StockActionButton nameButton={'Follow'} ticker={this.props.ticker} get={Platform.GetFollowStock} unAction={Platform.UnFollowStock} action={Platform.FollowStock} icon={FollowIcon} colorActivated={'#1F77B4'}/>
                        </div>
                    </CardActions>
                </Card>
            </div>);
    }
};

class StockRankingList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            stockList: [] 
        };
        this.loadDataFromServer(props);
    };

    loadDataFromServer(props) {
        var stockListTicker = [];
        props.stockList.map(
            (stock) => 
                stockListTicker.push(stock.ticker));
        
        Analytics.GetStockRankingApi.call({
            feature: props.featureId,
            stockTickerList: stockListTicker,
            to: props.to,
            from: props.from
        }).then(
            (data) => this.setState({ stockList: data })
            );
    }

    componentWillReceiveProps(nextProps) {
        this.loadDataFromServer(nextProps);
    }

    render() {
        var url = "./stock.html?ticker=" + this.props.ticker;
        return (
            <div className="paddingcard">
                <Card style="height: 500px;">
                    <CardText>
                        {/*<StockImage {...this.props}>
                        </StockImage>*/}

                        {/*<h1 className="stockTicker center" >
                            <a href={url}>{this.props.company}</a>
                        </h1>*/}
                    </CardText>
                    <CardActions>
                        <div className="right">
                            {/*<StockActionButton nameButton={'Like'} ticker={this.props.ticker} get={ Platform.GetLikeStock} unAction={Platform.UnLikeStock} action={Platform.LikeStock} icon={LikeIcon} colorActivated={'#D62728'}/>
                            <StockActionButton nameButton={'Follow'} ticker={this.props.ticker} get={Platform.GetFollowStock} unAction={Platform.UnFollowStock} action={Platform.FollowStock} icon={FollowIcon} colorActivated={'#1F77B4'}/>*/}
                        </div>
                    </CardActions>
                </Card>
            </div>);
    }
};


class StockList extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            stockList: []
        };
        this.loadDataFromServer(props);
    }

    loadDataFromServer(props) {
        Platform.GetStockList.call({
            fatureId: props.filter.fatureId,
            indexId: props.filter.indexId,
            to: props.filter.to,
            from: props.filter.from
        }).then(
            (data) => this.setState({ stockList: data })
            );
    }

    componentWillReceiveProps(nextProps) {
        this.loadDataFromServer(nextProps);
    }

    render() {
        return (<div className="rankingList">
            <StockRankingList stockList={this.state.stockList} featureId={this.props.filter.featureId} to={this.props.filter.to} from={this.props.filter.from} />
        </div>);
    };
};


class StockRanking extends React.Component {
    constructor(props) {
        super(props);
        var marketId = Urls.getParameterByName('market', document.location.href);
        this.state = {

        };

        Platform.GetMarket.call({ marketId: marketId }).then(
            (data) => {

                this.setState({
                    indexList: data.indexList,
                    market: data.market,
                    featureList: data.featureList,
                    filter: {
                        featureId: data.featureList[0],
                        indexId: data.indexList[0].ticker,
                        to: new Date(moment()),
                        from: new Date(moment().subtract(10, 'days'))
                    }
                });
            });
    };

    handleFilter(filter) {
        this.setState({
            filter: filter
        });
    }

    handleIndex(e) {
        var filter = this.state.filter;
        filter.indexId = e.props.value;
        this.setState({ filter: filter });
    };

    render() {

        if (this.state.market) {
            this.props.title(this.state.market.name);
            return (

                <div className="container">
                    <IndexList handle={(index) => this.handleIndex(index)} indexList={this.state.indexList}></IndexList>
                    <Analyzer onchange={(filter) => this.handleFilter(filter)} featureList={this.state.featureList} filter={this.state.filter}></Analyzer>
                    <StockList filter={this.state.filter}></StockList>
                </div>
            );
        } else {
            return (<div></div>);
        }



    }
};


window.onload = function () {


    ReactDOM.render(
        <Page menu='1' >
            <StockRanking ></StockRanking>
        </Page>, document.getElementById("main"));



};


