
var MapZoomMonitor = function (map, countryDataSet) {

    this.zoomFactor = 0.9;
    this.centered = null;
    this.width = map.options.element.clientWidth;
    this.height = map.options.element.clientHeight;

    this.reset = function () {
        this.centered = null;
        D3.selectAll("g")
                .transition()
                .duration(750)
                .style("stroke-width", "1.5px")
                .attr("transform", "");

    };

    this.addMarketList = function (d) {
        if (!countryDataSet[d.id])
            return;

        if (this.centered === d) {
            D3.selectAll(
                    '.datamaps-bubble'
                    )
                    .remove();
            return;
        }
        map.bubbles(countryDataSet[d.id].marketList, {
            popupTemplate: function (
                    geo,
                    data
                    ) {
                return '<div class="hoverinfo">' + data .description + '<img src="' + data .url + '">';

            }
        });

        D3.selectAll( '.datamaps-bubble' )
                .on('click',
                        function (d) {
                            document.location  .href ="ranking.html?market=" + d.id;
                        });
    };

    this.zoom = function (d) {
        var bounds;

        if (this.centered === d) {
            return this.reset();
        }
        console.log(d);
        this.centered = d;
        if (d.radius) { //Circle
            var cx = D3.select(D3.event.target)
                    .attr("cx");
            var cy = D3.select(D3.event.target)
                    .attr("cy");
            bounds = [
                [Number(cx) - d.radius, Number(cy) - d.radius],
                [Number(cx) + d.radius, Number(cy) + d.radius]
            ];
        } else {
            bounds = map.path.bounds(d);
        }

        var dx = bounds[1][0] - bounds[0][0],
                dy = bounds[1][1] - bounds[0][1],
                x = (bounds[0][0] + bounds[1][0]) / 2,
                y = (bounds[0][1] + bounds[1][1]) / 2,
                scale = this.zoomFactor / Math.max(dx / this.width, dy /
                        this.height),
                translate = [this.width / 2 - scale * x, this.height / 2 -
                            scale * y
                ];
        D3.selectAll("g")
                .transition()
                .duration(750)
                .style("stroke-width", 1.5 / scale + "px")
                .attr(
                    "transform", "translate(" + translate + ")scale(" + scale + ")"
                        );

    };
};

var createMap = function (mapElement, countryDataSet) {
    var map = new Datamap({
        element: mapElement,
        responsive: true,
        // countries don't listed in dataset will be painted with this color
        fills: {
            defaultFill: '#F5F5F5',
            blue: 'blue',
            green: '#A9D18E'
        },
        setProjection: function (element) {
            var projection = D3.geoEquirectangular()
                    // .center([300, 0])
                    .rotate([300, 0])
                    //  .scale(400)
                    .translate([element.offsetWidth / 2,
                        element.offsetHeight / 2
                    ]);
            var path = D3.geoPath()
                    .projection(projection);

            return {
                path: path,
                projection: projection
            };
        },
        data: countryDataSet,
        done: function (datamap) {
            var monitor = new MapZoomMonitor(datamap, countryDataSet);
            D3.selectAll('.datamaps-subunit')
                    .on('click', function (d) {
                        monitor.addMarketList(d);
                        monitor.zoom(d);
                    });

        },
        geographyConfig: {
            borderColor: '#DEDEDE',
            highlightBorderWidth: 2,
            // don't change color on mouse hover
            highlightFillColor: function (geo) {
                return geo['fillColor'] ||
                        '#F5F5F5';
            },
            // only change border
            highlightBorderColor: '#B7B7B7',
            // show desired information in tooltip

            popupTemplate: function (geo, data) {
                // don't show tooltip if country don't present in dataset
                if (!data) {
                    return;
                }
                // tooltip content
                return ['<div class="hoverinfo">',
                    '<strong>',
                    geo.properties.name,
                    '</strong>',
                    '<br>Market list: <strong>',
                    data.marketList[0].name,
                    '</strong>',
                    '</div>'
                ].join('');
            }
        }

    });

    var mapWindows = mapElement.children[0];

    mapWindows.style.position = '';
    mapWindows.style.height = mapWindows.width.baseVal.value * 0.40 + 'px';
    mapElement.style.paddingBottom = 0;
    return map;
};

window.onload = function () {

    ReactDOM.render(<Page  menu='1' title="World Market">
    <div id='map'></div>
    </Page>, document.getElementById("main"));

    Platform.GetMarketMap.call().then(
            function (data) {
                var dataSet = {};
                for(var key in data){
                    dataSet[key]={};
                    dataSet[key].fillKey = 'green';
                    dataSet[key].marketList =  data[key];
                    console.log(dataSet[key].marketList);
                    dataSet[key].marketList.forEach(function(market){
                        market.fillKey='blue';
                        market.radius= 0.7;
                        market.url= 'http://www.lepoint.fr/images/2015/03/13/bourse-ok-3126605-jpg_2765204_660x281.JPG';
                    });
                    
                }
                console.log(dataSet);
                // render map
                var mapElement = document.getElementById('map');
                createMap(mapElement, dataSet);
            }
    );



};