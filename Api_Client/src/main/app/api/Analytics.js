var Api = require('./util/Api');
var ApiMethod = require('./util/ApiMethod');
var StreamingMethod = require('./util/StreamingMethod');
var Authenticator = require('./util/Authenticator');
var Host = require('./Host');
var AnalyticsApi = function(url, method){
    this.call=function(request){ 
         return  new Authenticator().loader(new Api(url, method), request);
    };
    
    this.streaming=function(request, output){ 
        new Authenticator().connect(new Api(url, method), request, output);
    };
};

var Analytics={
    Host: Host + ':8800/api/analytics/'
};
Analytics.GetStockHistoryApi = new AnalyticsApi(Analytics.Host +'getStockHistory',  ApiMethod.Post);
Analytics.GetStockRankingApi = new AnalyticsApi(Analytics.Host +'getStockRanking',  ApiMethod.Post);
Analytics.QuoteStreaming = new AnalyticsApi(Analytics.Host +'streaming',  StreamingMethod.Subscribe);
Analytics.GetWebMetaApi = new AnalyticsApi(Analytics.Host +'getWebMeta',  ApiMethod.Post);

module.exports = Analytics;


